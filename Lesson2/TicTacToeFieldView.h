//
//  TicTacToeFieldView.h
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicTacToeCell.h"
#import "NSIndexPath+TicTacToeFieldView.h"

@class TicTacToeFieldView;

@protocol TicTacToeFieldViewDelegate <NSObject>

- (void)fieldView:(TicTacToeFieldView *)field didSelectCellAtIndexPath:(NSIndexPath *)indexPath;

@end

@protocol TicTacToeFieldViewDataSource <NSObject>

- (NSUInteger)fieldWidthForFieldView:(TicTacToeFieldView *)field;
- (NSUInteger)fieldHeightForFieldView:(TicTacToeFieldView *)field;
- (TicTacToeCellState)fieldView:(TicTacToeFieldView *)field stateForCellAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface TicTacToeFieldView : UIView

@property (readonly) NSUInteger fieldWidth;
@property (readonly) NSUInteger fieldHeight;

@property NSArray *cells;

@property (weak) id <TicTacToeFieldViewDelegate> delegate;
@property (weak, nonatomic) id <TicTacToeFieldViewDataSource> dataSource;

- (void)reloadData;

@end
