//
//  NSIndexPath+TicTacToeFieldView.m
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import "NSIndexPath+TicTacToeFieldView.h"

@implementation NSIndexPath (TicTacToeFieldView)

+ (NSIndexPath *)indexPathForRow:(NSInteger)row column:(NSInteger)column
{
    return [NSIndexPath indexPathForItem:row inSection:column];
}

- (NSInteger)column
{
    return self.section;
}

@end
