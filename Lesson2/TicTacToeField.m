//
//  TicTacToeField.m
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import "TicTacToeField.h"
#import "NSIndexPath+TicTacToeFieldView.h"

@interface TicTacToeField ()

@property (readwrite) NSUInteger width;
@property (readwrite) NSUInteger height;
@property (readwrite) NSArray *cells;

@end

@implementation TicTacToeField

- (instancetype)initWithWidth:(NSUInteger)width height:(NSUInteger)height
{
    self = [self init];
    if (self) {
        _width = width;
        _height = height;
        
        NSMutableArray *cells = [NSMutableArray new];
        NSUInteger numberOfCells = width * height;
        for (NSInteger index = 0; index < numberOfCells; index++) {
            TicTacToeCell *cell = [TicTacToeCell new];
            [cells addObject:cell];
        }
        _cells = cells;
    }
    return self;
}

- (TicTacToeCell *)cellForIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row * self.width + indexPath.column;
    return self.cells[index];
}

#pragma mark - Game

- (BOOL)canSelectCellAtIndexPath:(NSIndexPath *)indexPath
{
    TicTacToeCell *cell = [self cellForIndexPath:indexPath];
    BOOL isEmpty = (cell.state == TicTacToeCellStateEmpty);
    return isEmpty;
}

- (TicTacToeFieldGameState)gameState
{
    BOOL haveRedWon = [self havePlayerWithCellStateWon:TicTacToeCellStateCross];
    if (haveRedWon) {
        return TicTacToeFieldGameStateRedWon;
    }
    
    BOOL haveGreenWon = [self havePlayerWithCellStateWon:TicTacToeCellStateCircle];
    if (haveGreenWon) {
        return TicTacToeFieldGameStateGreenWon;
    }
    
    BOOL isDraw = YES;
    for (NSInteger index = 0; index < self.cells.count; index++) {
        TicTacToeCell *cell = self.cells[index];
        if (cell.state == TicTacToeCellStateEmpty) {
            isDraw = NO;
            break;
        }
    }
    if (isDraw) {
        return TicTacToeFieldGameStateDraw;
    }
    
    return TicTacToeFieldGameStateIncomplete;
}

- (BOOL)havePlayerWithCellStateWon:(TicTacToeCellState)cellState
{
    // Rows.
    for (NSInteger row = 0; row < self.height; row++) {
        BOOL haveWon = YES;
        for (NSInteger column = 0; column < self.width; column++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row column:column];
            TicTacToeCell *cell = [self cellForIndexPath:indexPath];
            if (cell.state != cellState) {
                haveWon = NO;
                break;
            }
        }
        if (haveWon) {
            return YES;
        }
    }
    
    // Columns.
    for (NSInteger column = 0; column < self.width; column++) {
        BOOL haveWon = YES;
        for (NSInteger row = 0; row < self.height; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row column:column];
            TicTacToeCell *cell = [self cellForIndexPath:indexPath];
            if (cell.state != cellState) {
                haveWon = NO;
                break;
            }
        }
        if (haveWon) {
            return YES;
        }
    }
    
    //
    BOOL haveWon = YES;
    for (NSInteger row = 0; row < self.height; row++) {
        NSInteger column = row;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row column:column];
        TicTacToeCell *cell = [self cellForIndexPath:indexPath];
        if (cell.state != cellState) {
            haveWon = NO;
            break;
        }
    }
    if (haveWon) {
        return YES;
    }
    
    haveWon = YES;
    for (NSInteger row = 0; row < self.height; row++) {
        NSInteger column = self.width - row - 1;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row column:column];
        TicTacToeCell *cell = [self cellForIndexPath:indexPath];
        if (cell.state != cellState) {
            haveWon = NO;
            break;
        }
    }
    if (haveWon) {
        return YES;
    }
    
    return NO;
}

- (void)restart
{
    for (NSInteger index = 0; index < self.cells.count; index++) {
        TicTacToeCell *cell = self.cells[index];
        cell.state = TicTacToeCellStateEmpty;
    }
}

@end
