//
//  TicTacToeCellView.h
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicTacToeCell.h"

@class TicTacToeCellView;

@protocol TicTacToeCellViewDelegate <NSObject>

- (void)cellViewDidPressed:(TicTacToeCellView *)cell;

@end

@interface TicTacToeCellView : UIView

@property (nonatomic) TicTacToeCellState state;

@property (weak) id <TicTacToeCellViewDelegate> delegate;

@end
