//
//  TicTacToeFieldView.m
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import "TicTacToeFieldView.h"
#import "TicTacToeCellView.h"

@interface TicTacToeFieldView () <TicTacToeCellViewDelegate>

@property (readwrite) NSUInteger fieldWidth;
@property (readwrite) NSUInteger fieldHeight;

@end

@implementation TicTacToeFieldView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self reloadData];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self reloadData];
    }
    return self;
}

- (void)setDataSource:(id<TicTacToeFieldViewDataSource>)dataSource
{
    _dataSource = dataSource;
    [self reloadData];
}

- (void)reloadData
{
    if (!self.dataSource) {
        return;
    }
    
    if (self.cells.count) {
        for (TicTacToeCellView *cell in self.cells) {
            [cell removeFromSuperview];
        }
    }
    
    self.fieldWidth = [self.dataSource fieldWidthForFieldView:self];
    self.fieldHeight = [self.dataSource fieldHeightForFieldView:self];
    NSMutableArray *cells = [NSMutableArray new];
    NSUInteger numberOfCells = self.fieldHeight * self.fieldWidth;
    for (NSInteger index = 0; index < numberOfCells; index++) {
        NSIndexPath *indexPath = [self indexPathForCellIndex:index];
        TicTacToeCellState state = [self.dataSource fieldView:self stateForCellAtIndexPath:indexPath];
        
        CGRect frame = CGRectZero;
        frame.size.width = self.frame.size.width / self.fieldWidth;
        frame.size.height = self.frame.size.height / self.fieldHeight;
        frame.origin.x = indexPath.row * frame.size.width;
        frame.origin.y = indexPath.column * frame.size.height;
        TicTacToeCellView *cell = [[TicTacToeCellView alloc] initWithFrame:frame];
        cell.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        cell.state = state;
        cell.delegate = self;
        [cells addObject:cell];
        [self addSubview:cell];
    }
    self.cells = cells;
}

- (NSIndexPath *)indexPathForCellIndex:(NSInteger)index
{
    NSInteger column = index % self.fieldWidth;
    NSInteger row = index / self.fieldWidth;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row column:column];
    return indexPath;
}

- (void)cellViewDidPressed:(TicTacToeCellView *)cell
{
    NSInteger index = [self.cells indexOfObject:cell];
    NSIndexPath *indexPath = [self indexPathForCellIndex:index];
    if (self.delegate) {
        [self.delegate fieldView:self didSelectCellAtIndexPath:indexPath];
    }
}

@end
