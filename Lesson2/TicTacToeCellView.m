//
//  TicTacToeCellView.m
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import "TicTacToeCellView.h"

@implementation TicTacToeCellView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:(NSCoder *)aDecoder];
    if (self) {
        [self configureView];
        [self update];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configureView];
        [self update];
    }
    return self;
}

- (void)configureView
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [self addGestureRecognizer:tap];
}

- (void)tap:(id)sender
{
    if (self.delegate) {
        [self.delegate cellViewDidPressed:self];
    }
}

- (void)setState:(TicTacToeCellState)state
{
    _state = state;
    [self update];
}

- (void)update
{
    self.backgroundColor = [TicTacToeCellView colorForState:self.state];
}

+ (UIColor *)colorForState:(TicTacToeCellState)state
{
    switch (state) {
        case TicTacToeCellStateEmpty: return [UIColor whiteColor];
        case TicTacToeCellStateCross: return [UIColor redColor];
        case TicTacToeCellStateCircle: return [UIColor greenColor];
        default: return [UIColor blackColor];
    }
}

@end
