//
//  ViewController.m
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import "ViewController.h"
#import "TicTacToeField.h"
#import "TicTacToeFieldView.h"

#define FIELD_SIZE 3

@interface ViewController () <TicTacToeFieldViewDataSource, TicTacToeFieldViewDelegate>

@property TicTacToeFieldView *fieldView;
@property TicTacToeField *field;
@property UIButton *restartButton;
@property BOOL isCurrentPlayerRed;
@property BOOL isGameFinished;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor grayColor];
    
    // Field.
    self.field = [[TicTacToeField alloc] initWithWidth:FIELD_SIZE height:FIELD_SIZE];
    
    // Field view.
    CGFloat size = MIN(self.view.frame.size.width, self.view.frame.size.height) * 0.8;
    self.fieldView = [[TicTacToeFieldView alloc] initWithFrame:CGRectMake(0.0, 0.0, size, size)];
    self.fieldView.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    self.fieldView.dataSource = self;
    self.fieldView.delegate = self;
    [self.view addSubview:self.fieldView];
    
    // Restart.
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(0, 0.0, self.view.frame.size.width - 50, 30);
    button.center = CGPointMake(self.view.frame.size.width / 2.0, 100.0);
    [button setTitle:@"RESTART" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(restartButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.restartButton = button;
    [self.view addSubview:self.restartButton];
}

- (void)restartButtonPressed:(id)sender
{
    [self restart];
}

- (void)restart
{
    self.isGameFinished = NO;
    [self.field restart];
    [self.fieldView reloadData];
}

#pragma mark - Data Source

- (NSUInteger)fieldWidthForFieldView:(TicTacToeFieldView *)field
{
    return self.field.width;
}

- (NSUInteger)fieldHeightForFieldView:(TicTacToeFieldView *)field
{
    return self.field.height;
}

- (TicTacToeCellState)fieldView:(TicTacToeFieldView *)field stateForCellAtIndexPath:(NSIndexPath *)indexPath
{
    TicTacToeCell *cell = [self.field cellForIndexPath:indexPath];
    return cell.state;
}

- (void)fieldView:(TicTacToeFieldView *)field didSelectCellAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isGameFinished) {
        return;
    }
    
    TicTacToeCell *cell = [self.field cellForIndexPath:indexPath];
    if ([self.field canSelectCellAtIndexPath:indexPath]) {
        cell.state = self.isCurrentPlayerRed ? TicTacToeCellStateCross : TicTacToeCellStateCircle;
        self.isCurrentPlayerRed = !self.isCurrentPlayerRed;
        [self.fieldView reloadData];
    }
    
    TicTacToeFieldGameState gameState = self.field.gameState;
    if (gameState == TicTacToeFieldGameStateIncomplete) {
        return;
    }
    self.isGameFinished = YES;
    
    NSString *message;
    switch (gameState) {
        case TicTacToeFieldGameStateDraw: message = @"Draw!"; break;
        case TicTacToeFieldGameStateRedWon: message = @"Red Won!"; break;
        case TicTacToeFieldGameStateGreenWon: message = @"Green Won!"; break;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

@end
