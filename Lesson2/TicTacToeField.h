//
//  TicTacToeField.h
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TicTacToeCell.h"

typedef NS_ENUM(NSUInteger, TicTacToeFieldGameState) {
    TicTacToeFieldGameStateIncomplete,
    TicTacToeFieldGameStateRedWon,
    TicTacToeFieldGameStateGreenWon,
    TicTacToeFieldGameStateDraw,
    TicTacToeFieldGameStatesCount
};

@interface TicTacToeField : NSObject

@property (readonly) NSUInteger width;
@property (readonly) NSUInteger height;
@property (readonly) NSArray *cells;

- (instancetype)initWithWidth:(NSUInteger)width height:(NSUInteger)height;

- (TicTacToeCell *)cellForIndexPath:(NSIndexPath *)indexPath;

// Game.
- (BOOL)canSelectCellAtIndexPath:(NSIndexPath *)indexPath;
@property (readonly) TicTacToeFieldGameState gameState;
- (void)restart;

@end