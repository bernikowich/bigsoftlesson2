//
//  NSIndexPath+TicTacToeFieldView.h
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSIndexPath (TicTacToeFieldView)

@property (readonly) NSInteger column;

+ (NSIndexPath *)indexPathForRow:(NSInteger)row column:(NSInteger)column;

@end
