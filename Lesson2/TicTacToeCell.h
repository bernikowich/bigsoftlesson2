//
//  TicTacToeCell.h
//  Lesson2
//
//  Created by Timur Bernikowich on 29.03.15.
//  Copyright (c) 2015 Timur Bernikowich. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TicTacToeCellState) {
    TicTacToeCellStateEmpty,
    TicTacToeCellStateCross,
    TicTacToeCellStateCircle,
    TicTacToeCellStatesCount
};

@interface TicTacToeCell : NSObject

@property TicTacToeCellState state;

@end
